using System;

public class StoryNode
{
    public string History;
    public string[] Answers;
    //public bool IsFinal;
    public bool IsIncorrect;
    public StoryNode[] NextNode;
    

}
