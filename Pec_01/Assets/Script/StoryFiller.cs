
public static class StoryFiller
{
    
    public static StoryNode FillStory()
    {
        var root = CreateNode(
            "Hola Haters",
            new[] {
            "Biden",
            "Putin"});

        var rootA = CreateNode(
            "Que comience el duelo",
            new[] {
            "Vamos Biden!"});

        var rootB = CreateNode(
            "Que comience el duelo",
            new[] {
            "Vamos Putin!"});

        var node1 = CreateNode(
            "Putin escribe:" + "\n" + "�Has dejado ya de usar pa�ales?" + "\n" +
            "Biden responde:",
            new[] {
            "�Por qu�? �Acaso quer�as pedir uno prestado?",
            "S� que las hay, s�lo que nunca las has aprendido.",
            "Me alegra que asistieras a tu reuni�n familiar diaria.",
            "Primero deber�as dejar de usarla como un plumero."});

        var node2 = CreateNode(
            "Putin escribe:" + "\n" + "�No hay palabras para describir lo asqueroso que eres!" + "\n" +
            "Biden responde:",
            new[] {
            "S� que las hay, s�lo que nunca las has aprendido.",
            "Me alegra que asistieras a tu reuni�n familiar diaria.",
            "Primero deber�as dejar de usarla como un plumero.",
            "Qu� apropiado, t� peleas como una vaca."});

        var node3 = CreateNode(
           "Putin escribe:" + "\n" + "�He hablado con simios m�s educados que tu!" + "\n" +
            "Biden responde:",
            new[] {
            "Me alegra que asistieras a tu reuni�n familiar diaria.",
            "Primero deber�as dejar de usarla como un plumero.",
            "Qu� apropiado, t� peleas como una vaca.",
            "Ya te est�n fastidiando otra vez las almorranas, �Eh?"});

        var node4 = CreateNode(
           "Putin escribe:" + "\n" + "�Llevar�s mi espada como si fueras un pincho moruno!" + "\n" +
            "Biden responde:",
           new[] {
            "Primero deber�as dejar de usarla como un plumero.",
            "Qu� apropiado, t� peleas como una vaca.",
            "Ya te est�n fastidiando otra vez las almorranas, �Eh?",
            "Ah, �Ya has obtenido ese trabajo de barrendero?"});

        var node5 = CreateNode(
           "Putin escribe:" + "\n" + "�Luchas como un ganadero! " + "\n" +
            "Biden responde:",
           new[] {
            "Qu� apropiado, t� peleas como una vaca.",
            "Ya te est�n fastidiando otra vez las almorranas, �Eh?",
            "Ah, �Ya has obtenido ese trabajo de barrendero?",
            "Y yo tengo un SALUDO para ti, �Te enteras?"});

        var node6 = CreateNode(
            "Biden escribe:" + "\n" + "�Mi pa�uelo limpiar� tu sangre!" + "\n" +
            "Putin responde:",
             new[] {
             "Ah, �Ya has obtenido ese trabajo de barrendero?",
             "Y yo tengo un SALUDO para ti, �Te enteras?",
             "Te habr� ense�ado todo lo que sabes.",
             "�TAN r�pido corres?"});
        
        var node7 = CreateNode(
            "Biden escribe:" + "\n" + "�Una vez tuve un perro m�s listo que tu!" + "\n" +
            "Putin responde:",
           new[] {
             "Te habr� ense�ado todo lo que sabes.",
             "�TAN r�pido corres?",
             "Me haces pensar que alguien ya lo ha hecho.",
             "Quer�a asegurarme de que estuvieras a gusto conmigo."});

        var node8 = CreateNode(
           "Biden escribe:" + "\n" + "�Nadie me ha sacado sangre jam�s, y nadie lo har�!" + "\n" +
            "Putin responde:",
            new[] {
             "�TAN r�pido corres?",
             "Me haces pensar que alguien ya lo ha hecho.",
             "Quer�a asegurarme de que estuvieras a gusto conmigo.",
             "Qu� pena me da que nadie haya o�do hablar de ti"});

        var node9 = CreateNode(
           "Biden escribe:" + "\n" + "�Me das ganas de vomitar!" + "\n" +
            "Putin responde:",
           new[] {
             "Me haces pensar que alguien ya lo ha hecho.",
             "Quer�a asegurarme de que estuvieras a gusto conmigo.",
             "Qu� pena me da que nadie haya o�do hablar de ti",
             "�Incluso antes de que huelan tu aliento?"});

        var node10 = CreateNode(
           "Biden escribe:" + "\n" + "�Tienes los modales de un mendigo!" + "\n" +
            "Putin responde:",
           new[] {
             "Quer�a asegurarme de que estuvieras a gusto conmigo.",
             "Qu� pena me da que nadie haya o�do hablar de ti",
             "�Incluso antes de que huelan tu aliento?",
             "Estar�a acabado si la usases alguna vez."});

        var nodeA = CreateNode(
            "Respuesta incorrecta",
            new[] {
             "Siguiente oponente"});

        var nodeB = CreateNode(
            "Respuesta incorrecta",
            new[] {
             "Siguiente oponente"});

        var nodeC = CreateNode(
          "El duelo ha terminado",
            new[] {
             "Fin"});

        root.NextNode[0] = rootA;
        root.NextNode[1] = rootB;

        rootA.NextNode[0] = node1;
        rootB.NextNode[0] = node6;

        nodeA.NextNode[0] = rootB;
        nodeB.NextNode[0] = rootA;

        node1.NextNode[0] = node2;
        //node2.IsCorrect1 = true;
        node1.NextNode[1] = nodeA;
        node1.NextNode[2] = nodeA;
        node1.NextNode[3] = nodeA;

        node2.NextNode[0] = node3;
        //node3.IsCorrect2 = true;
        node2.NextNode[1] = nodeA;
        node2.NextNode[2] = nodeA;
        node2.NextNode[3] = nodeA;

        node3.NextNode[0] = node4;
        //node4.IsCorrect3 = true;
        node3.NextNode[1] = nodeA;
        node3.NextNode[2] = nodeA;
        node3.NextNode[3] = nodeA;

        node4.NextNode[0] = node5;
        node4.NextNode[1] = nodeA;
        node4.NextNode[2] = nodeA;
        node4.NextNode[3] = nodeA;

        node5.NextNode[0] = nodeC;
        node5.NextNode[1] = nodeA;
        node5.NextNode[2] = nodeA;
        node5.NextNode[3] = nodeA;

        node6.NextNode[0] = node7;
        node6.NextNode[1] = nodeB;
        node6.NextNode[2] = nodeB;
        node6.NextNode[3] = nodeB;

        node7.NextNode[0] = node8;
        node7.NextNode[1] = nodeB;
        node7.NextNode[2] = nodeB;
        node7.NextNode[3] = nodeB;

        node8.NextNode[0] = node9;
        node8.NextNode[1] = nodeB;
        node8.NextNode[2] = nodeB;
        node8.NextNode[3] = nodeB;

        node9.NextNode[0] = node10;
        node9.NextNode[1] = nodeB;
        node9.NextNode[2] = nodeB;
        node9.NextNode[3] = nodeB;

        node10.NextNode[0] = nodeC;
        node10.NextNode[1] = nodeB;
        node10.NextNode[2] = nodeB;
        node10.NextNode[3] = nodeB;

        //nodeC.IsFinal = true;
        nodeA.IsIncorrect = true;
        return root;
    }


    private static StoryNode CreateNode(string history, string[] options)
    {
        var node = new StoryNode
        {
            History = history,
            Answers = options,
            NextNode = new StoryNode[options.Length]
        };
        return node;
    }


}


